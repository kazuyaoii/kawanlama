<?php

class Barang extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Barang_model', 'm');
        $this->load->model('Pembelian_model', 'p');
    }

    function index() {
        $data = array(
            'content' => 'barang_form',
            'judul' => 'hajdhka',
            'subjudul' => 'hkk',
            'barang' => $this->m->get(),
            'pembelian' => $this->p->get()
        );
        $this->load->view('template/content', $data);
    }
    
    function save(){
        $this->m->save($this->input->post());
        echo json_encode(array("status" => TRUE));
        }
    
    function del($id_barang) {
        $this->m->del($id_barang);
        redirect('barang');
    }

    function edit($id_barang) {
        $data = $this->m->find($id_barang);
        echo json_encode($data);
    }

    function update() {
        $id_barang = $this->input->post('id_barang');
        $data = $this->input->post();
        $this->m->update($id_barang, $data);
        echo json_encode(array("status" => TRUE));
    }    
}
