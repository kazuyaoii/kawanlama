<?php

class Pembelian extends CI_Controller {
     function __construct() {
        parent:: __construct();
        $this->load->model('Pembelian_model', 'm');
    }

    function index(){
        $data = array(
            'content' => 'pembelian_form',
            'judul' => 'hajdhka',
            'subjudul' => 'hkk',
            'pembelian' => $this->m->get()
        );
        $this->load->view('template/content', $data);
    }
    
    function save(){
        $this->m->save($this->input->post());
        echo json_encode(array("status" => TRUE));
        }
    
    function del($id_pembelian) {
        $this->m->del($id_pembelian);
        redirect('pembelian');
    }

    function edit($id_pembelian) {
        $data = $this->m->find($id_pembelian);
        echo json_encode($data);
    }

    function update() {
        $id_pembelian = $this->input->post('id_pembelian');
        $data = $this->input->post();
        $this->m->update($id_pembelian, $data);
        echo json_encode(array("status" => TRUE));
    }
//    function get_harga($id){
//          $data = array(
//            'barang' => $this->m->get_harga($id)
//        );
//        $this->load->view('harga_barang', $data);
//    }
    
}
