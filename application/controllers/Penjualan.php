<?php

class Penjualan extends CI_Controller {
    
    function __construct() {
        parent:: __construct();
        $this->load->model('Penjualan_model', 'm');
        $this->load->model('Barang_model', 'm');
    }

    function index(){
        $data = array(
            'content' => 'penjualan_form',
            'judul' => 'hajdhka',
            'subjudul' => 'hkk',
            'penjualan' => $this->m->get(),
            'barang' => $this->m->get_barang()
        );
        $this->load->view('template/content', $data);
    }
    
    function save(){
        $this->m->save($this->input->post());
        echo json_encode(array("status" => TRUE));
        }
    
    function del($id_penjualan) {
        $this->m->del($id_penjualan);
        redirect('penjualan');
    }

    function edit($id_penjualan) {
        $data = $this->m->find($id_penjualan);
        echo json_encode($data);
    }

    function update() {
        $id_penjualan = $this->input->post('id_penjualan');
        $data = $this->input->post();
        $this->m->update($id_penjualan, $data);
        echo json_encode(array("status" => TRUE));
    }
    function get_harga($id){
          $data = array(
            'barang' => $this->m->get_harga($id)
        );
        $this->load->view('harga_barang', $data);
    }
    
}
