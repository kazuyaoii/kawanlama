<?php

class Barang_model extends CI_Model {

    function get() {
        return $this->db->get('barang')->result_array();
    }

    function save($data) {
        $this->db->insert('barang', $data);
    }

    function del($id_barang) {
        $this->db->where('id_barang', $id_barang)->delete('barang');
    }

    function update($id_barang, $data) {
        $this->db->where('id_barang', $id_barang)->update('barang', $data);
    }

    function find($id_barang) {
        return $this->db->where('id_barang', $id_barang)->get('barang')->row_array();
    }

     function get_pembelian() {
        return $this->db->get('pembelian')->result_array();
    }
}
