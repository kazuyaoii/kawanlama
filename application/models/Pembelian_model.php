<?php

class Pembelian_model extends CI_Controller {

    function get() {
        return $this->db->get('pembelian')->result_array();
    }

    function save($data) {
        $this->db->insert('pembelian', $data);
    }

    function del($id_pembelian) {
        $this->db->where('id_pembelian', $id_pembelian)->delete('pembelian');
    }

    function update($id_pembelian, $data) {
        $this->db->where('id_pembelian', $id_pembelian)->update('pembelian', $data);
    }

    function find($id_pembelian) {
        return $this->db->where('id_pembelian', $id_pembelian)->get('pembelian')->row_array();
    }

}    

