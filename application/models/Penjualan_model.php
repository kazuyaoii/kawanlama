<?php

class Penjualan_model extends CI_Controller {

    function get() {
        return $this->db->get('penjualan')->result_array();
    }

    function save($data) {
        $this->db->insert('penjualan', $data);
    }

    function del($id_penjualan) {
        $this->db->where('id_penjualan', $id_penjualan)->delete('penjualan');
    }

    function update($id_penjualan, $data) {
        $this->db->where('id_penjualan', $id_penjualan)->update('penjualan', $data);
    }

    function find($id_penjualan) {
        return $this->db->where('id_penjualan', $id_penjualan)->get('penjualan')->row_array();
    }

    function get_barang() {
        return $this->db->get('barang')->result_array();
    }

    function get_harga($id) {
        $sql = $this->db->get_where('barang', array('id_barang' => $id));
        $harga = 0;
        if ($sql->num_rows() > 0) {
            foreach ($sql->result() as $b) {
                $harga = $b->harga;
            }
            return $harga;
        } else {
            return -1;
        }
    }

}
