<button type="button" onclick="tambah()" class="btn btn-primary" >
    Tambah Data
</button>
<table class="table table-striped">

    <thead>
        <tr class="info">
            <th>ID BARANG</th>
            <th>ID PEMBELIAN</th>
            <th>NAMA BARANG</th>
            <th>JENIS BARANG</th>
            <th>UKURAN</th>
            <th>NOMER RANGKA</th>
            <th>HARGA</th>
            <th>NOMER RANGKA</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($barang as $r) { ?>
            <tr>
                <td><?= $r['id_barang'] ?></td>
                <td><?= $r['id_pembelian'] ?></td>
                <td><?= $r['nama'] ?></td>
                <td><?= $r['jenis'] ?></td>
                <td><?= $r['ukuran'] ?></td>
                <td><?= $r['nomer_rangka'] ?></td>    
                <td><?= $r['harga'] ?></td>    
                <td><?= $r['stok'] ?></td>
                <td>
<!--                    <a class="btn btn-warning btn-sm" href="<?= site_url() ?>/barang/edit/<?= $r['id_barang'] ?> ">
                        <span class="glyphicon glyphicon-edit"></span> Ubah
                    </a>-->
                    <a class="btn btn-warning btn-sm" onclick="edit_barang(<?= $r['id_barang'] ?>)">
                        <span class="glyphicon glyphicon-edit"></span> Ubah
                    </a>
                    <a class="btn btn-danger btn-sm del" href="<?= site_url() ?>/barang/del/<?= $r['id_barang'] ?>">
                        <span class="glyphicon glyphicon-trash"></span> Hapus
                    </a>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>


<form action="" method="post" id="form">
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="judul">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" id="header">
                    <button type="reset" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="judul">Form Barang</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="id_barang">Id Barang</label>
                        <input type="text" class="form-control" name="id_barang" id="id_barang" placeholder="Id barang tidak boleh kosong" required="">
                    </div>
                    <div class="form-group">
                        <label for="nama">Nama Barang</label>
                        <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama barang tidak boleh kosong" required="">
                    </div>
                    <div>
                        <div class="form-group">
                            <label for="jenis">Jenis Barang</label>
                            <select class="form-control" name="jenis" required>
                                <option value="" selected="" disabled="">==Pilih Jenis Barang==</option>
                                <?php foreach ($pembelian as $p) { ?>
                                <option value="<?= $p['id_pembelian'] ?>"> <?= $p['jenis'] ?>  </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="ukuran">Ukuran Barang</label>
                        <input type="text" class="form-control" name="ukuran" id="ukuran" placeholder="Ukuran barang tidak boleh kosong" required="">
                    </div>
                    <div class="form-group">
                        <label for="nomer_rangka">Nomer Rangka</label>
                        <input type="text" class="form-control" name="nomer_rangka" id="nomer_rangka" placeholder="Nomor rangka barang tidak boleh kosong" required="">
                    </div>
                    <div class="form-group">
                        <label for="harga">Harga Barang</label>
                        <input type="text" class="form-control" name="harga" id="harga" placeholder="Harga barang tidak boleh kosong" required="">
                    </div>
                    <div class="form-group">
                        <label for="stok">Stok Barang</label>
                        <input type="text" class="form-control" name="stok" id="stok" placeholder="Stok barang tidak boleh kosong" required="">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                    <button type="button" class="btn btn-primary">Batal</button>
                    <button type="button" id="btnSimpan" class="btn btn-success" onclick="simpan()">Simpan</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
//    $('.table').DataTable();
    var save_method='';
    function edit_barang(id){
        save_method='update';
        $.ajax({
            url:"<?= site_url('barang/edit')?>/"+id,
            type:"GET",
            dataType:"JSON",
            success:function(data){
                $('#id_barang').val(data.id_barang);
                $('#nama').val(data.nama);
                $('#ukuran').val(data.ukuran);
                $('#nomer_rangka').val(data.nomer_rangka);
                $('#harga').val(data.harga);
                $('#stok').val(data.stok);
                $('#myModal').modal('show');
                $(".modal-title").text('Edit Data');
                $("#btnSimpan").text('Update');
            }
          
        });
       
        
    }
    function simpan(){
        var url;
        if(save_method=='add'){
            url=" <?= site_url('barang/save') ?>";
        }
        if(save_method=='update'){
            url="<?= site_url('barang/update') ?>";
        }
        
        var formData = new FormData($('#form')[0]);
        $.ajax({
            url: url,
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "JSON",
            success: function (data) {
                        location.reload();
                    }
        });
    }
            
    function tambah(){
        save_method='add';
        $("#form")[0].reset();
        $(".modal-title").text('Tambah Data');
        $("#myModal").modal('show');
    
    }
            
    
    
    $('#myModal').on('shown.bs.modal', function () {
        $('#myInput').focus()
    })
</script>
</form>