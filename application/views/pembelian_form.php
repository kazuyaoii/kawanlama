<button type="button" onclick="tambah()" class="btn btn-primary" >
    Tambah Data
</button>

<!--tabel pembelian-->
<table class="table dataTable table-responsive table-hover  table-striped">

    <thead>
        <tr class="info">
            <th>ID PEMBELIAN</th>
            <th>NAMA BARANG</th>
            <th>JENIS</th>
            <th>HARGA</th>
            <th>JUMLAH</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($pembelian as $r) { ?>
            <tr>
                <td><?= $r['id_pembelian'] ?></td>
                <td><?= $r['nama'] ?></td>
                <td><?= $r['jenis'] ?></td>
                <td><?= $r['harga'] ?></td>
                <td><?= $r['jumlah'] ?></td>    
                <td>
                    <a class="btn btn-warning btn-sm" onclick="edit_pembelian(<?= $r['id_pembelian'] ?>)">
                        <span class="glyphicon glyphicon-edit"></span> Ubah
                    </a>
                    <a class="btn btn-danger btn-sm del" href="<?= site_url() ?>/pembelian/del/<?= $r['id_pembelian'] ?>">
                        <span class="glyphicon glyphicon-trash"></span> Hapus
                    </a>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>

<!--Form pembelian-->
<form action="" method="post" id="form">
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="judul">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" id="header">
                    <button type="reset" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="judul">Form Pembelian</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="id_pembelian">Id Pembelian</label>
                        <input type="text" class="form-control" name="id_pembelian" id="id_pembelian" placeholder="Id pembelian tidak boleh kosong" required="">
                    </div>
                    <div class="form-group">
                        <label for="jumlah">Nama Barang</label>
                        <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama barang pembelian tidak boleh kosong" required="">
                    </div>
                    <div class="form-group">
                        <label for="jumlah">Jenis Barang</label>
                        <input type="text" class="form-control" name="jenis" id="jenis" placeholder="Jenis barang pembelian tidak boleh kosong" required="">
                    </div>
                    <div class="form-group">
                        <label for="harga">Harga</label>
                        <input type="text" class="form-control" name="harga" id="harga" placeholder="Harga tidak boleh kosong" required="">
                    </div>
                    <div class="form-group">
                        <label for="jumlah">Jumlah Barang</label>
                        <input type="text" class="form-control" name="jumlah" id="jumlah" placeholder="Jumlah barang tidak boleh kosong" required="">
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                    <button type="button" class="btn btn-primary">Batal</button>
                    <button type="button" id="btnSimpan" class="btn btn-success" onclick="simpan()">Simpan</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var save_method='';
    function edit_pembelian(id){
        save_method='update';
        $.ajax({
            url:"<?= site_url('pembelian/edit')?>/"+id,
            type:"GET",
            dataType:"JSON",
            success:function(data){
                $('#id_pembelian').val(data.id_pembelian);
                $('#nama').val(data.nama);
                $('#jenis').val(data.jenis);
                $('#harga').val(data.harga);
                $('#jumlah').val(data.jumlah);
                $('#myModal').modal('show');
                $(".modal-title").text('Edit Data');
                $("#btnSimpan").text('Update');
            }
          
        });
       
        
    }
    function simpan(){
        var url;
        if(save_method=='add'){
            url=" <?= site_url('pembelian/save') ?>";
        }
        if(save_method=='update'){
            url="<?= site_url('pembelian/update') ?>";
        }
        
        var formData = new FormData($('#form')[0]);
        $.ajax({
            url: url,
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "JSON",
            success: function (data) {
                        location.reload();
                    }
        });
    }
            
    function tambah(){
        save_method='add';
        $("#form")[0].reset();
        $(".modal-title").text('Tambah Data');
        $("#myModal").modal('show');
    
    }
    function harga(){
        var id = $('#id_barang').val();
        $.ajax({
            url: "<?= site_url('pembelian/get_harga') ?>/"+id,
            type: 'POST',
            success: function (data) {
                       $('#harga').html(data);
                    }
        });
    }
            
    
    
    $('#myModal').on('shown.bs.modal', function () {
        $('#myInput').focus()
    })
</script>
</form>