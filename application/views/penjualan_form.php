<button type="button" onclick="tambah()" class="btn btn-primary" >
    Tambah Data
</button>

<!--tabel penjualan-->
<table class="table dataTable table-responsive table-hover  table-striped">

    <thead>
        <tr class="info">
            <th>ID PENJUALAN</th>
            <th>ID BARANG</th>
            <th>NAMA BARANG</th>
            <th>HARGA</th>
            <th>JUMLAH BARANG DIBELI</th>
            <th>QTY</th>
            <!--<th>SUBTOTAL</th>-->
            <th>TOTAL</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($penjualan as $r) { ?>
            <tr>
                <td><?= $r['id_penjualan'] ?></td>
                <td><?= $r['id_barang'] ?></td>
                <td><?= $r['nama'] ?></td>
                <td><?= $r['harga'] ?></td>
                <td><?= $r['jumlah'] ?></td>    
                <td><?= $r['qty'] ?></td>
                <td><?= $subtotal = $r['harga'] * $r['jumlah'] ?></td>
                <!--<td><?= $r['total'] ?></td>-->
                <td>
                    <a class="btn btn-warning btn-sm" onclick="edit_penjualan(<?= $r['id_penjualan'] ?>)">
                        <span class="glyphicon glyphicon-edit"></span> Ubah
                    </a>
                    <a class="btn btn-danger btn-sm del" href="<?= site_url() ?>/penjualan/del/<?= $r['id_penjualan'] ?>">
                        <span class="glyphicon glyphicon-trash"></span> Hapus
                    </a>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>

<!--Form penjualan-->
<form action="" method="post" id="form">
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="judul">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" id="header">
                    <button type="reset" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="judul">Form Penjualan</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="id_penjualan">Id Penjualan</label>
                        <input type="text" class="form-control" name="id_penjualan" id="id_penjualan" placeholder="Id penjualan tidak boleh kosong" required="">
                    </div>
                    <div class="form-group">
                        <label for="nama">Nama Barang</label>
                        <select name="id_barang" class="form-control" id="id_barang" onchange="harga()">
                            <option disabled selected>==Pilih Barang==</option>
                            <?php foreach ($barang as $b): ?>
                            <option value="<?= $b['id_barang'] ?>"><?= $b['nama']?></option>
                            <?php endforeach?>
                        </select>
                    </div>
                    <div id="harga">
                        
                        
                    </div>
                    <div class="form-group">
                        <label for="jumlah">Jumlah Barang</label>
                        <input type="text" class="form-control" name="jumlah" id="jumlah" placeholder="Nomor rangka penjualan tidak boleh kosong" required="">
                    </div>
                    <div class="form-group">
                        <label for="qty">QTY</label>
                        <input type="text" class="form-control" name="qty" id="qty" placeholder="QTY tidak boleh kosong" required="">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                    <button type="button" class="btn btn-primary">Batal</button>
                    <button type="button" id="btnSimpan" class="btn btn-success" onclick="simpan()">Simpan</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var save_method='';
    function edit_penjualan(id){
        save_method='update';
        $.ajax({
            url:"<?= site_url('penjualan/edit')?>/"+id,
            type:"GET",
            dataType:"JSON",
            success:function(data){
                $('#id_penjualan').val(data.id_penjualan);
                $('#id_barang').val(data.id_barang);
                $('#nama').val(data.nama);
                $('#harga').val(data.harga);
                $('#jumlah').val(data.jumlah);
                $('#qty').val(data.qty);
                $('#subtotal').val(data.subtotal);
                $('#total').val(data.total);
                $('#myModal').modal('show');
                $(".modal-title").text('Edit Data');
                $("#btnSimpan").text('Update');
            }
          
        });
       
        
    }
    function simpan(){
        var url;
        if(save_method=='add'){
            url=" <?= site_url('penjualan/save') ?>";
        }
        if(save_method=='update'){
            url="<?= site_url('penjualan/update') ?>";
        }
        
        var formData = new FormData($('#form')[0]);
        $.ajax({
            url: url,
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "JSON",
            success: function (data) {
                        location.reload();
                    }
        });
    }
            
    function tambah(){
        save_method='add';
        $("#form")[0].reset();
        $(".modal-title").text('Tambah Data');
        $("#myModal").modal('show');
    
    }
    function harga(){
        var id = $('#id_barang').val();
        $.ajax({
            url: "<?= site_url('penjualan/get_harga') ?>/"+id,
            type: 'POST',
            success: function (data) {
                       $('#harga').html(data);
                    }
        });
    }
            
    
    
    $('#myModal').on('shown.bs.modal', function () {
        $('#myInput').focus()
    })
</script>
</form>