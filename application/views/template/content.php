<?php 
    $this->load->view('template/header');
    $this->load->view('template/asidebar');
    
?> 
<!--main content start-->
            <section id="main-content">
                <section class="wrapper">            
                    <!--overview start-->
                    <div class="row">
                        <div class="col-lg-12">
                            <h3 class="page-header"><i class="fa fa-laptop"></i> <?= $judul ?></h3>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-home"></i><a href="#">Home</a></li>
                                <li><i class="fa fa-laptop"></i><?= $subjudul ?></li>						  	
                            </ol>
                        </div>
                    </div>

                    <?php 
                        $this->load->view($content);
                    ?>
                </section>
                
            <!--main content end-->
 <?php
 $this->load->view('template/footer');
 ?>