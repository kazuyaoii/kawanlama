<script type="text/javascript" >
    //untuk memanggil kelas datatables
    $('table').DataTable({
        responsive: true,
        dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-6'Bi><'col-sm-6'p>>",
        buttons: ['excel', 'copy', 'pdf']
    });

</script>

</body>
</html>