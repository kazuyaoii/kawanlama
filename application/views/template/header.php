<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
        <meta name="author" content="GeeksLabs">
        <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
        <link rel="shortcut icon" href="img/favicon.png">

        <title>Kawan lama</title>

        <!--sweetalert bootstrap css-->
        <link rel="stylesheet" href="<?= base_url() ?>sweetalert/sweetalert.css"/>
        <!--datatable bootstrap css-->
        <link rel="stylesheet" href="<?= base_url() ?>datatables/datatables.min.css"/>
        <!-- Bootstrap CSS -->    
        <link href="<?= base_url() ?>css/bootstrap.min.css" rel="stylesheet">
        <!-- bootstrap theme -->
        <link href="<?= base_url() ?>css/bootstrap-theme.css" rel="stylesheet">
        <!--external css-->
        <!-- font icon -->
        <link href="<?= base_url() ?>css/elegant-icons-style.css" rel="stylesheet" />
        <link href="<?= base_url() ?>css/font-awesome.min.css" rel="stylesheet" />    
        <!-- full calendar css-->
        <link href="<?= base_url() ?>assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
        <link href="<?= base_url() ?>assets/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
        <!-- easy pie chart-->
        <link href="<?= base_url() ?>assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
        <!-- owl carousel -->
        <link rel="stylesheet" href="<?= base_url() ?>css/owl.carousel.css" type="text/css">
        <link href="<?= base_url() ?>css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
        <!-- Custom styles -->
        <link rel="stylesheet" href="<?= base_url() ?>css/fullcalendar.css">
        <link href="<?= base_url() ?>css/widgets.css" rel="stylesheet">
        <link href="<?= base_url() ?>css/style.css" rel="stylesheet">
        <link href="<?= base_url() ?>css/style-responsive.css" rel="stylesheet" />
        <link href="<?= base_url() ?>css/xcharts.min.css" rel=" stylesheet">	
        <link href="<?= base_url() ?>css/jquery-ui-1.10.4.min.css" rel="stylesheet">

        <!--javascript-->
        <!--<script src="<?= base_url() ?>js/jquery.js"></script>-->
        <script src="<?= base_url() ?>jquery-1.12.3.min.js"></script>
        <!--sweetalert javascript-->
        <script src="<?= base_url() ?>sweetalert/sweetalert.min.js"></script>
        <!--datatable javasript-->
        <script src="<?= base_url() ?>datatables/datatables.min.js"></script>
        <script src="<?= base_url() ?>js/jquery-ui-1.10.4.min.js"></script>
        <script src="<?= base_url() ?>js/jquery-1.8.3.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>js/jquery-ui-1.9.2.custom.min.js"></script>
        <!-- bootstrap -->
        <script src="<?= base_url() ?>js/bootstrap.min.js"></script>
        <!-- nice scroll -->
        <script src="<?= base_url() ?>js/jquery.scrollTo.min.js"></script>
        <script src="<?= base_url() ?>js/jquery.nicescroll.js" type="text/javascript"></script>
        <!-- charts scripts -->
        <script src="<?= base_url() ?>assets/jquery-knob/js/jquery.knob.js"></script>
        <script src="<?= base_url() ?>js/jquery.sparkline.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
        <script src="<?= base_url() ?>js/owl.carousel.js" ></script>
        <!-- jQuery full calendar -->
        <<script src="<?= base_url() ?>js/fullcalendar.min.js"></script> <!-- Full Google Calendar - Calendar -->
        <script src="<?= base_url() ?>assets/fullcalendar/fullcalendar/fullcalendar.js"></script>
        <!--script for this page only-->
        <script src="<?= base_url() ?>js/calendar-custom.js"></script>
        <script src="<?= base_url() ?>js/jquery.rateit.min.js"></script>
        <!-- custom select -->
        <script src="<?= base_url() ?>js/jquery.customSelect.min.js" ></script>
        <script src="<?= base_url() ?>assets/chart-master/Chart.js"></script>

        <!--custome script for all page-->
        <script src="<?= base_url() ?>js/scripts.js"></script>
        <!-- custom script for this page-->
        <script src="<?= base_url() ?>js/sparkline-chart.js"></script>
        <script src="<?= base_url() ?>js/easy-pie-chart.js"></script>
        <script src="<?= base_url() ?>js/jquery-jvectormap-1.2.2.min.js"></script>
        <script src="<?= base_url() ?>js/jquery-jvectormap-world-mill-en.js"></script>
        <script src="<?= base_url() ?>js/xcharts.min.js"></script>
        <script src="<?= base_url() ?>js/jquery.autosize.min.js"></script>
        <script src="<?= base_url() ?>js/jquery.placeholder.min.js"></script>
        <script src="<?= base_url() ?>js/gdp-data.js"></script>	
        <script src="<?= base_url() ?>js/morris.min.js"></script>
        <script src="<?= base_url() ?>js/sparklines.js"></script>	
        <script src="<?= base_url() ?>js/charts.js"></script>
        <script src="<?= base_url() ?>js/jquery.slimscroll.min.js"></script>


    </head>

    <body>
        <!-- container section start -->
        <section id="container" class="">


            <header class="header dark-bg">
                <div class="toggle-nav">
                    <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i class="icon_menu"></i></div>
                </div>

                <!--logo start-->
                <a href="#" class="logo">Kawan <span class="lite">lama</span></a>
                <!--logo end-->


                <div class="top-nav notification-row">                
                    <!-- notificatoin dropdown start-->
                    <ul class="nav pull-right top-menu">

                        <!-- user login dropdown start-->
                        <li class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <span class="profile-ava">
                                    <img alt="" src="<?= base_url() ?>img/admin.jpg" width="50" height="50">
                                </span>
                                <span class="username">ADMIN KAWAN LAMA STORE</span>
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu extended logout">
                                <div class="log-arrow-up"></div>
                                <li class="eborder-top">
                                    <a href="#"><i class="icon_profile"></i> My Profile</a>
                                </li>
                                <li>
                                    <a href="<?= base_url() ?>Login/index.php"><i class="icon_key_alt"></i> Log Out</a>
                                </li>
                            </ul>
                        </li>
                        <!-- user login dropdown end -->
                    </ul>
                    <!-- notificatoin dropdown end-->
                </div>
            </header>      
            <!--header end-->

